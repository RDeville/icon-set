# Colorize all -colorize R,G,B color into the -fill "sRGB(R,G,B)" color
# Sample : 
convert -fill "sRGB(0,255,0)" -colorize 100 img.png img.png
# This line convert all white color into green in img.png

# For convertion to svg, first convert to pnm then use potrace
convert img.png img.pnm
potrace img.pnm -s -o img.svg
